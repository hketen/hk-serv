/**
 * Created by hakan on 04/07/2017.
 */
import {Client} from 'socket.io';

import {SocketController} from '../..';

export class ExampleMessageController extends SocketController {

    handleMessage(client: Client): void {

        // this.io
        // this.socket

        // client

        console.log('ExampleMessageController.handleMessage: ', client);
    }

    handleRegister(client: Client): void {
        console.log('ExampleMessageController.handleRegister: ', client);
    }

    handleJoin(client: Client): void {
        console.log('ExampleMessageController.handleJoin: ', client);
    }

    handleLeave(client: Client): void {
        console.log('ExampleMessageController.handleLeave: ', client);
    }

    handleGetChatRooms(client: Client): void {
        console.log('ExampleMessageController.handleGetChatRooms: ', client);
    }

    handleGetAvailableUsers(client: Client): void {
        console.log('ExampleMessageController.handleGetAvailableUsers: ', client);
    }

}
