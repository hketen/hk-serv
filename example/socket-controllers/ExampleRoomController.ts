/**
 * Created by hakan on 04/07/2017.
 */
import {Client} from 'socket.io';

import {SocketController} from '../..';

export class ExampleRoomController extends SocketController {

    welcomeRoom(client: Client): void {
        console.log('this.socket.id: ', this.socket.id);
        // this.socket.to(this.socket.id).emit('welcomeRoom', 'Sende hoş geldin çocuum');
        console.log('welcomeRoom: ', client);
    }

    joinRoom(client: Client): void {

        this.socket.broadcast.emit('joinRoom', client);
        console.log('ExampleRoomController.joinRoom: ', client);
    }

    leaveRoom(client: Client): void {
        console.log('ExampleRoomController.leaveRoom: ', client);
    }

}
