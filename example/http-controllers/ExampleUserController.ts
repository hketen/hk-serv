/**
 * Created by hakan on 04/07/2017.
 */

import {HttpController, IBlueprintRoutes} from '../..';

export class ExampleUserController extends HttpController implements IBlueprintRoutes {

    async create(): Promise<void> {
        console.log(this.request);

        this.ok({ok: 'create'});
    }

    async destroy(): Promise<void> {
        this.ok({ok: 'destroy'});
    }

    async item(): Promise<void> {
        console.log(this.request);
        this.ok({ok: 'item'});
    }

    /**
     * @apiVersion 1.0.0
     * @src {get} /user Example List
     * @apiName UserList
     * @apiGroup User
     * @apiDescription Örnek Açıklama
     *
     * @apiParam {String} username Kullanıcı Adı
     * @apiParam {String} password Parola
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "token": "TOKEN STRING",
     *          "user": "user object"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/list
     */
    async items(): Promise<void> {

        // this.requireParameters(['username', 'password']);
        //
        // const username = this.getParam('username');
        // const password = this.getParam('password');

        this.response.send({
            users: [
                'Hakan Keten',
                'Erk Bamyacı',
                'Barış Özdemir',
                'Anıl Türk',
            ],
        });
    }

    async update(): Promise<void> {
        this.ok({ok: 'update'});
    }

    async partialUpdate(): Promise<void> {
        this.ok({ok: 'partialUpdate'});
    }

}
