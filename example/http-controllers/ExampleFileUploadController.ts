/**
 * Created by hakan on 04/07/2017.
 */
import {HttpController} from '../..';

export class ExampleFileUploadController extends HttpController {

    /**
     * @apiVersion 1.0.0
     * @src {get} /upload File Upload Example
     * @apiName Upload
     * @apiGroup File
     * @apiDescription Örnek Açıklama
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "token": "TOKEN STRING",
     *          "user": "user object"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /upload
     */
    public async upload(): Promise<void> {

        await this.uploadHandler([
            {name: 'images', maxCount: 10},
            {name: 'videos', maxCount: 3}
        ]);

        const files: { uploaded: any[], notUploaded: any[] } = this.getFiles(['images', 'videos']);

        this.ok(files);

    }

}
