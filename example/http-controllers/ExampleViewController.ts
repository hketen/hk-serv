/**
 * Created by hakan on 04/07/2017.
 */
import {HttpController} from '../..';

export class ExampleViewController extends HttpController {

    public index(): void {

        try {

            this.view('index', {
                names: [
                    'Hakan',
                    'Keten',
                    'Deneme',
                ],
            });

        } catch (e) {

            console.log('e: ', e);

        }
    }

}
