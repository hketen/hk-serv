/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes, Method} from '../..';
import {ExampleViewController} from '../http-controllers/ExampleViewController';

export class IndexRoute {

    public static create(): any[] {

        return CoreRoutes.routes(
            ExampleViewController,
            {method: Method.get, routePath: '/', fnName: 'index'}
        );
    }

}
