/**
 * Created by hakan on 04/07/2017.
 */

import {CoreRoutes} from '../..';
import {ExampleUserController} from '../http-controllers/ExampleUserController';

export class UserRoute {

    public static create(): any[] {

        return CoreRoutes.blueprintRoutes(ExampleUserController, '/user');
    }

}
