/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes, Method} from '../..';
import {ExampleFileUploadController} from '../http-controllers/ExampleFileUploadController';

export class FileUploadRoute {

    public static create(): any[] {

        return CoreRoutes.routes(
            ExampleFileUploadController,
            {
                routePath: '/upload',
                method: Method.post,
                fnName: 'upload'
            }
        );
    }

}
