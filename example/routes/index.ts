import {FileUploadRoute} from './FileUploadRoute';
import {IndexRoute} from './IndexRoute';
import {UserRoute} from './UserRoute';

/**
 * Created by hakan on 11/05/2019.
 */

export default [
    IndexRoute.create(),
    UserRoute.create(),
    FileUploadRoute.create()
];
