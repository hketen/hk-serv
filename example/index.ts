import {ApplicationServer} from '..';

import Routers from './routes';
import Sockets from './sockets';
import './styles';

class App extends ApplicationServer {

    async registerRoutes(): Promise<any[]> {
        return Routers;
    }

    async registerSockets(): Promise<any[]> {
        return Sockets;
    }
}

(async (): Promise<void> => {

    const app: App = new App();

    await app.listen();

})();
