/**
 * Created by hakan on 04/07/2017.
 */

import {Packet} from 'socket.io';

import {CoreSockets, SocketEntity} from '../..';
import {ExampleRoomController} from '../socket-controllers/ExampleRoomController';

export abstract class RoomSocket {

    public static create(): SocketEntity {

        return CoreSockets.events(
            ExampleRoomController,
            [
                (packet: Packet, next: (err?: any) => void): void => {
                    console.log('middleware');
                    next();
                }
            ],
            {event: 'welcomeRoom', fnName: 'welcomeRoom'},
            {event: 'joinRoom', fnName: 'joinRoom'},
            {event: 'leaveRoom', fnName: 'leaveRoom'},
        );
    }
}
