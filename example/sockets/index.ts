import {MessageSocket} from './MessageSocket';
import {RoomSocket} from './RoomSocket';

/**
 * Created by hakan on 11/05/2019.
 */

export default [
    {
        namespace: '/application',
        controllers: [
            MessageSocket.create(),
            RoomSocket.create()
        ]
    }
];
