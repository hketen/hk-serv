/**
 * Created by hakan on 04/07/2017.
 */

import {CoreSockets, SocketEntity} from '../..';
import {ExampleMessageController} from '../socket-controllers/ExampleMessageController';

export abstract class MessageSocket {

    public static create(): SocketEntity {

        return CoreSockets.events(
            ExampleMessageController,
            [],
            {event: 'register', fnName: 'handleRegister'},
            {event: 'join', fnName: 'handleJoin'},
            {event: 'leave', fnName: 'handleLeave'},
            {event: 'message', fnName: 'handleMessage'},
            {event: 'chatRooms', fnName: 'handleGetChatRooms'},
            {event: 'availableUsers', fnName: 'handleGetAvailableUsers'},
        );
    }
}
