import {NextFunction, Request, Response} from 'express';

export const toAsyncHandler: any = (fn: any): any => (error: any, req: Request, res: Response, next: NextFunction): void => {

    if (error) {

        next(error);

        return;
    }

    Promise
        .resolve(fn(req, res, next))
        .catch(next);
};
