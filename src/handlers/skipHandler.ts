import {NextFunction, Request, Response} from 'express';

export const skipHandler: any = (): any => (error: any, req: Request, res: Response, next: NextFunction): void => {
    if (error) {
        console.log(error);
        return;
    }

    next();
};
