import {NextFunction, Request, Response} from 'express';

import {ErrorModel} from '../models';

export const invalidParameters: any = (): any => (error: any, req: Request, res: Response, next: NextFunction): void => {

    if (error !== null) {

        res.json(new ErrorModel('INVALID_PARAMETER_FORMAT', 200));
        return;
    }

    next();
};
