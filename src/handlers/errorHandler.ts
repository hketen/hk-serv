import {NextFunction, Request, Response} from 'express';

import {ErrorModel} from '../models';

export const errorHandler: any = (): any => (err: any, req: Request, res: Response, next: NextFunction): void => {
    if (err) {
        const error: ErrorModel = new ErrorModel(err);

        res
            .status(error.status)
            .send(error);
    } else {
        next();
    }
};
