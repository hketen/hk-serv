import {NextFunction, Request, Response} from 'express';

export const xhrErrorHandler: any = (): any => (err: any, req: Request, res: Response, next: NextFunction): void => {
    if (req.xhr) {
        res.status(500).send({error: 'UNKNOWN_ERROR'});
    } else {
        next(err);
    }
};
