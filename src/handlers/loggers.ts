import {RequestHandler} from 'express';
import fs from 'fs';
import logger from 'morgan';

import {Config, rootPathResolve} from '../utils';

import {skipHandler} from './skipHandler';

const accessLogs: string = Config.get('accessLogs', 'access.log');

export const loggers: () => RequestHandler = (): any => {
    if (process.env.DISABLED_LOGGER) {
        return skipHandler();
    }

    if (process.env.dev) {
        return logger('dev');
    }

    return logger('common', {
        stream: fs.createWriteStream(rootPathResolve(accessLogs), {flags: 'a'}),
    });
};
