import {NextFunction, Request, Response} from 'express';

export const skipMiddleware: any = (req: Request, res: Response, next: NextFunction): void => next();
