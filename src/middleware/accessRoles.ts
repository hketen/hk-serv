import {NextFunction, Response} from 'express';

import {HttpController} from '../controllers';
import {ErrorModel} from '../models';

export const accessRoles: any = (roles: string[]): any => (req: any, res: Response, next: NextFunction): void => {
    const user: any = req.user;

    if (HttpController.isRoles(user, roles)) {
        next();
    } else {
        next(new ErrorModel('ACCESS_DENIED', 401));
    }
};
