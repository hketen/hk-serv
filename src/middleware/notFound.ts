import {NextFunction, Response} from 'express';
import fs from 'fs';

import {Config, rootPathResolve} from '../utils';

const publicFolder: string = Config.get('server.publicFolder') || 'public';

export const notFound: any = (req: any, res: Response, next: NextFunction): void => {
    const indexFile: string = rootPathResolve(publicFolder, 'index.html');

    if (fs.existsSync(indexFile)) {
        res.sendFile(indexFile);
    } else {
        res
            .status(404)
            .send('Not Found!');
    }
};
