import {NextFunction, Request, Response} from 'express';

export const tokenPrepare: any = (req: Request, res: Response, next: NextFunction): void => {

    req.headers.token = req.headers.token || req.query.token || false;
    next();
};
