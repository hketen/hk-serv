import {NextFunction, Request, Response} from 'express';

export const xPoweredBy: any = (req: Request, res: Response, next: NextFunction): void => {

    res.header('X-Powered-By', 'Hakan KETEN');
    next();
};
