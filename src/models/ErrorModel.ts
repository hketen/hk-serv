/**
 * Created by hakan on 19/05/2018.
 */
import * as _ from 'lodash';

export class ErrorModel {

    public status: number;
    public error: string;
    public details: any;

    constructor(error: any, status: number = 500, details?: any) {
        if (ErrorModel.instanceOf(error) || ErrorModel.instanceOf(details)) {
            this.status = error.status || 500;
            this.error = error.error || details.error;
            this.details = error.details || (details && details.details) || null;
        } else {
            this.status = error.status || status;
            this.error = _.isString(error) ? error : error.message;
            this.details = _.isString(error) ? details : error.stack || null;
        }

        this.checkSerialize();
    }

    public static instanceOf(error: any): boolean {
        return error instanceof ErrorModel;
    }

    private checkSerialize(): void {
        JSON.stringify(this.error);
        JSON.stringify(this.details);
    }
}
