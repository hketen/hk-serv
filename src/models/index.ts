import {DatabaseModel} from './DatabaseModel';
import {ErrorModel} from './ErrorModel';

export {DatabaseModel, ErrorModel};
