/**
 * Created by hakan on 19/05/2018.
 */

export class DatabaseModel {

    public hosts: string[];
    public dbName: string;
    public username: string;
    public password: string;
    public options: any[];

    constructor(cfg?: any) {
        this.hosts = cfg.hosts;
        this.dbName = cfg.dbName;
        this.username = cfg.username;
        this.password = cfg.password;
        this.options = cfg.options || [];
    }
}
