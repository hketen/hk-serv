import _ from 'lodash';

export const isUndefinedOrEmpty: (value: any) => boolean = (value: any): boolean => {
    const undef: boolean = _.isUndefined(value);
    const empty: boolean = _.isString(value) && _.isEmpty(value);
    return (undef || empty);
};

export const wait: (second: number) => Promise<void> = (second: number): Promise<void> => {

    return new Promise<void>((resolve: () => void): any => setTimeout(resolve, second * 1000));
};

export const waitWithRandom: (minSecond: number, maxSecond: number) => Promise<void> = (minSecond: number, maxSecond: number): Promise<void> => {
    return wait(_.random(minSecond, maxSecond, false));
};
