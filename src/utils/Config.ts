/**
 * Created by hakan on 04/07/2017.
 */
import _ from 'lodash';

const cfg: { development: any, production: any } = require('./config.json');

export class Config {

    private constructor() {
    }

    public static get(sub: string | null = null, def?: any): any {

        const config: object = (process.env.dev ? cfg.development : cfg.production) || cfg;

        return sub == null ? config : _.result(config, sub, def);
    }

    public static find<T>(key: string, predicate: any): T {

        const value: any[] = Config.get(key);
        const item: any = _.find<T>(value, predicate);
        if (!item) {
            throw new Error('Config not Found!');
        }

        return item;
    }
}
