export class Converter {

    public static sizeToBytes(size: string): number {

        const sizes: string[] = ['BYTES', 'KB', 'MB', 'GB', 'TB'];

        for (let i: number = 0; i < sizes.length; i++) {
            const sizeSuffix: string = sizes[i];

            if (!size.toUpperCase().includes(sizeSuffix)) {
                continue;
            }

            const n: number = parseInt(size.toUpperCase().replace(sizeSuffix, ''), 10);
            return i === 0 ? n : n * Math.pow(1024, i);
        }

        return -1;
    }
}
