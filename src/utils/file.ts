import {path as appRootPath} from 'app-root-path';
import fs from 'fs';
import mkdirp from 'mkdirp';
import path from 'path';
import util from 'util';

const mkdirpAsync: (dir: string) => Promise<void> = util.promisify(mkdirp);
const unlinkAsync: (file: string) => Promise<void> = util.promisify(fs.unlink);
const readFileAsync: (file: string) => Promise<Buffer> = util.promisify(fs.readFile);
const writeFileAsync: (file: string, text: string) => Promise<void> = util.promisify(fs.writeFile);
const appendFileAsync: (file: string, text: string) => Promise<void> = util.promisify(fs.appendFile);

export const mkdir: (dir: string) => Promise<void> = async (dir: string): Promise<void> => {
    if (!fs.existsSync(dir)) {
        await mkdirpAsync(dir);
    }
};

export const unlink: (file: string) => Promise<void> = async (file: string): Promise<void> => {
    await unlinkAsync(file);
};

export const write: (file: string, text: string) => Promise<void> = async (file: string, text: string): Promise<void> => {
    await writeFileAsync(file, text);
};

export const read: (file: string) => Promise<Buffer> = async (file: string): Promise<Buffer> => {
    return readFileAsync(file);
};

export const append: (file: string, text: string) => Promise<void> = async (file: string, text: string): Promise<void> => {
    await appendFileAsync(file, text);
};

export const pathResolve: (...paths: string[]) => string = (...paths: string[]): string => {
    const file: string = path.join(...paths);
    return path.resolve(path.normalize(file));
};

export const rootPathResolve: (...paths: string[]) => string = (...paths: string[]): string => {
    const file: string = path.join(appRootPath, ...paths);
    return path.resolve(path.normalize(file));
};
