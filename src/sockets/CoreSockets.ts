/**
 * Created by hakan on 04/07/2017.
 */
import {Packet} from 'socket.io';

import {SocketEntity, SocketEventEntity} from '../entity';

export abstract class CoreSockets {

    public static events(CurrentClass: any, middleware?: Array<(packet: Packet, next: (err?: any) => void) => void>, ...args: SocketEventEntity[]): SocketEntity {

        return {CurrentClass, middleware, events: args};

    }
}
