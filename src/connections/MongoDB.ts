/**
 * Created by hakan on 04/07/2017.
 */

import {Db, MongoClient, MongoError} from 'mongodb';

import {DatabaseModel, ErrorModel} from '../models';

export class MongoDB {

    private constructor(private config: DatabaseModel) {
    }

    private static dbs: { [key: string]: Db } = {};

    public static async connect(config: DatabaseModel): Promise<Db> {
        const connectionUrl: string = MongoDB.getConnectUrl(config);

        if (!MongoDB.dbs[connectionUrl]) {

            MongoDB.dbs[connectionUrl] = await new MongoDB(config).connect();
        }

        return MongoDB.dbs[connectionUrl];
    }

    public static getConnectUrl(cfg: DatabaseModel): string {
        // mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]

        const optionsUrlParams: string = this.generateOptionsUrlParams(cfg);
        return `mongodb://${cfg.username}:${cfg.password}@${cfg.hosts.join(',')}/${cfg.dbName}${optionsUrlParams}`;
    }

    private static generateOptionsUrlParams(cfg: DatabaseModel): string {

        const options: string[] = [];
        for (let i: number = 0; i < (cfg.options || []).length; i++) {
            options.push(cfg.options[i].join('='));
        }

        const optsUrl: string = options.join('&');

        return optsUrl.length > 0 ? '?' + optsUrl : '';
    }

    public async connect(): Promise<Db> {
        const connectionUrl: string = MongoDB.getConnectUrl(this.config);

        return new Promise((resolve: (db: Db) => void, reject: any): void => {

            MongoClient.connect(connectionUrl, {
                native_parser: true,
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }, (err: MongoError, client: MongoClient) => {

                if (err) {
                    reject(err);

                    return;
                }

                const db: Db = client.db(this.config.dbName);

                if (db) {
                    resolve(db);
                } else {
                    reject(new ErrorModel('UNKNOWN_DATABASE_CONNECTION_ERROR', 200));
                }
            });
        });
    }
}
