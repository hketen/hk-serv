/**
 * Created by hakan on 04/07/2017.
 */

import low, {AdapterSync, LowdbSync} from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';

import {rootPathResolve} from '../utils';

export class LowDB {

    private static dbs: { [key: string]: LowdbSync<AdapterSync> } = {};

    private constructor(private filePath: string) {
    }

    public static async connect(filePath: string): Promise<LowdbSync<AdapterSync>> {

        if (!LowDB.dbs[filePath]) {

            LowDB.dbs[filePath] = await new LowDB(filePath || '/db.json').connect();
        }

        return LowDB.dbs[filePath];
    }

    public async connect(): Promise<LowdbSync<AdapterSync>> {
        const storagePath: string = rootPathResolve(this.filePath);
        const adapter: AdapterSync = new FileSync(storagePath, {
            defaultValue: {},
            serialize: (data: any): string => JSON.stringify(data),
            deserialize: (dataStr: string): any => JSON.parse(dataStr)
        });

        const db: LowdbSync<AdapterSync> = low(adapter);
        db.read();

        return db;
    }

}
