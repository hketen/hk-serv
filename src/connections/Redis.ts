/**
 * Created by hakan on 04/07/2017.
 */

import {ObjectID} from 'bson';
import _ from 'lodash';
import redis from 'redis';

export class Redis {

    private static instance: Redis;
    private static debug: boolean;

    private constructor(private url: string) {
    }

    private client: any;

    public static useRedis(url: string): void {
        Redis.instance = new Redis(url);
        Redis.instance.init();
        console.log('using redis');
    }

    public static getInstance(debug?: boolean): Redis {
        if (!this.instance) {
            throw new Error('before run Redis.useRedis(url)');
        }

        Redis.debug = !!debug;

        return this.instance;
    }

    private init(): void {
        this.client = redis.createClient(this.url);
    }

    public async set(key: string, value: any): Promise<void> {
        const val: string = _.isString(value) ? value : JSON.stringify(value);

        return new Promise<void>((resolve: () => void, reject: any): void => {
            this.client.set(key, val, (err: any) => {
                if (Redis.debug) {
                    console.log('set:', err ? err : `${key}: ${val.toString().substring(0, 50)}`);
                }

                err ? reject(err) : resolve();
            });
        });
    }

    public async get(key: string, ids: string[] = []): Promise<any> {
        return new Promise<any>((resolve: (value: any) => void, reject: (error: any) => void): void => {
            this.client.get(key, (err: any, result: string) => {
                if (Redis.debug) {
                    console.log('get:', err ? err : `${key}: ${result && result.substring(0, 50)}`);
                }

                if (err) {
                    return reject(err);
                }

                try {

                    const value: any = JSON.parse(result);

                    _.forEach(ids, (id: string) => {

                        const idValues: any = _.result(value, id);
                        if (_.isArray(idValues)) {

                            const mapValues: any[] = _.map(idValues, (subId: string) => new ObjectID(subId));

                            _.set(value, id, mapValues);

                        } else if (_.isString(idValues)) {

                            _.set(value, id, new ObjectID(idValues));

                        }
                    });

                    resolve(value);

                    return;
                } catch (e) {

                }

                resolve(result);
            });
        });
    }

    public async clear(keys: string | string[]): Promise<void> {
        if (_.isString(keys)) {
            await this.set(keys, null);
        } else {
            _.forEach(keys, async (key: string) => {

                await this.set(key, null);
            });
        }
    }

}
