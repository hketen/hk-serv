/**
 * Created by hakan on 04/07/2017.
 */
/**
 * Created by hakan on 11/05/2019.
 */

import * as _ from 'lodash';
import {createTransport} from 'nodemailer';

import {IEmailConfig, IEmailTemplate} from '../entity';
import {ErrorModel} from '../models';
import {waitWithRandom} from '../utils';

export abstract class CoreEmail {

    private readonly mailConfig: IEmailConfig;
    private readonly mailTemplate: IEmailTemplate;

    private smtpTransport: any;
    private separatedMail: boolean = false;
    private to: string[] = [];
    private cc: string[] = [];
    private sendMsg: any[] = [];
    private senderType: 'otp' | 'transport' = 'transport';
    private subject: string = '';
    private params: any;
    private html: string = '';

    protected constructor(config: IEmailConfig, template: IEmailTemplate) {
        this.mailConfig = config;
        this.mailTemplate = template;
    }

    public addTo(mail: string): this {
        this.to.push(mail);

        return this;
    }

    public setTo(mails: string[]): this {
        this.to = mails;

        return this;
    }

    public addCc(mail: string): this {
        this.cc.push(mail);

        return this;
    }

    public setCc(mail: string[]): this {
        this.cc = mail;

        return this;
    }

    public clearCc(): this {
        this.cc = [];

        return this;
    }

    public setSenderType(type: 'otp' | 'transport'): this {
        this.senderType = type;

        return this;
    }

    public setSubject(subject: string): this {
        this.subject = subject;

        return this;
    }

    setSeparatedMail(separatedMail: boolean): this {
        this.separatedMail = separatedMail;

        return this;
    }

    isSeparatedMail(): boolean {
        return this.separatedMail;
    }

    public setParams(params: any): this {
        this.params = params;

        return this;
    }

    public addParam(param: string, value: any): this {
        if (!this.params) {
            this.params = {};
        }

        this.params[param] = value;

        return this;
    }

    public async send(): Promise<void> {
        if (this.to.length === 0) {
            throw new ErrorModel('SEND_TO_UNDEFINED', 200);
        }

        // mail adreslerini birleştirerek hepsinin tek seferde gönderilmesini sağlar.
        if (!this.isSeparatedMail()) {
            this.to = [this.to.join(',')];
        }

        this.prepareTemplate();

        this.initTransport();

        // OTP mi? Transport mu?
        await this.sendLoop(0);
    }

    private initTransport(): void {
        if (!this.mailConfig) {
            return;
        }

        const transportConfig: any = {
            direct: true,
            host: this.mailConfig.host,
            port: this.mailConfig.port,
            auth: {
                user: this.mailConfig.authUser,
                pass: this.mailConfig.authPass
            },
            secure: this.mailConfig.secure || false
        };

        this.smtpTransport = createTransport(transportConfig);
    }

    private prepareTemplate(): void {
        if (!this.mailTemplate) {
            return;
        }

        this.html = this.mailTemplate['html'];

        const keys: string[] = _.keys(this.params);

        for (const key of keys) {
            const value: string = this.params[key];

            this.html = this.html.replace(`{{${key}}}`, value);
        }

    }

    private async sendLoop(index: number): Promise<any[]> {
        if (index < this.to.length) {

            const to: string = this.to[index];

            // otp skipped.
            if (this.senderType === 'transport') {
                await waitWithRandom(10, 60);
            }

            await this.sendWithTo(to);

            await this.sendLoop(index + 1);

        } else {
            return Promise.resolve(this.sendMsg);
        }

        return [];
    }

    private async sendWithTo(to: string): Promise<void> {
        return new Promise((resolve: () => void): void => {
            if (!this.mailConfig) {
                return;
            }

            const mailOptions: any = {
                from: `"${this.mailConfig.name}" <${this.mailConfig.authUser}>`,
                to,
                cc: this.cc.join(','),
                subject: this.subject,
                // text: `Buraya text girebilirsiniz..`,
                html: this.html
            };

            this.smtpTransport.sendMail(mailOptions, (error: any, info: any) => {
                this.sendMsg.push({error, info});
                resolve();
            });

        });
    }
}
