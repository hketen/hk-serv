/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction} from 'express';
import {get, map} from 'lodash';

import {BlueprintMiddlewareEntity, Method, RouteEntity, RouteParamEntity} from '../entity';
import {toAsyncMiddleware} from '../middleware';

export abstract class CoreRoutes {

    public static route(CurrentClass: any, route: RouteParamEntity): RouteEntity {
        const actions: any[] = map([
            ...get(route, 'middleware', []),

            async (req: Request, res: Response, next: NextFunction): Promise<any> => {

                const newClass: any = new CurrentClass(req, res, next);

                return newClass[route.fnName]();

            }
        ], toAsyncMiddleware);

        return {
            method: route.method,
            routePath: route.routePath,
            actions
        };
    }

    public static routes(CurrentClass: any, ...args: RouteParamEntity[]): RouteEntity[] {
        return map(args, (route: RouteParamEntity) => CoreRoutes.route(CurrentClass, route));
    }

    public static blueprintRoutes(CurrentClass: any, routePath: string, middleware?: BlueprintMiddlewareEntity): RouteEntity[] {

        const routeModels: RouteParamEntity[] = [
            {method: Method.get, routePath: `${routePath}`, fnName: 'items'},
            {method: Method.get, routePath: `${routePath}/:id`, fnName: 'item'},
            {method: Method.post, routePath: `${routePath}`, fnName: 'create'},
            {method: Method.put, routePath: `${routePath}/:id`, fnName: 'update'},
            {method: Method.patch, routePath: `${routePath}/:id`, fnName: 'partialUpdate'},
            {method: Method.delete, routePath: `${routePath}/:id`, fnName: 'destroy'},
        ];

        const routes: RouteParamEntity[] = [];

        for (const route of routeModels) {

            const currentMiddleware: any[] = [
                ...(
                    get(middleware, 'all', [])
                ),
                ...(
                    get(middleware, route.fnName, [])
                )
            ];

            routes.push({middleware: currentMiddleware, ...route});
        }

        return CoreRoutes.routes(CurrentClass, ...routes);
    }

}
