/**
 * Created by hakan on 04/07/2017.
 */

import {
    Collection,
    Db,
    DeleteWriteOpResultObject,
    InsertOneWriteOpResult,
    InsertWriteOpResult,
    MongoError,
    ObjectID,
    UpdateWriteOpResult
} from 'mongodb';

import {ErrorModel} from '../models';

import {IMongoCoreDao} from './IMongoCoreDao';

export abstract class MongoCoreDao<T> implements IMongoCoreDao<T> {

    public readonly collectionName: string;
    protected collection: Collection;
    protected db: Db;

    protected constructor(db: Db, collectionName: string) {
        this.db = db;
        this.collectionName = collectionName;
        this.collection = db.collection(this.collectionName);
    }

    public getCollection(): Collection {
        return this.collection;
    }

    public getItem(filter: any, opts?: object): Promise<T | null> {

        return this.collection.findOne(filter, opts || {});
    }

    public getItemWithId(id: ObjectID, opts?: object): Promise<T | null> {

        return this.collection.findOne({_id: id}, opts || {});
    }

    public getItems(aggregateFilter?: any[]): Promise<T[]> {

        return new Promise((resolve: (data: any[]) => void, reject: (error: ErrorModel) => void): void => {
            this.collection.aggregate(aggregateFilter || []).toArray((err: MongoError, result: any[]) => {
                if (err) {
                    reject(new ErrorModel('LIST_NOT_READING', 200, err));
                } else {
                    resolve(result);
                }
            });
        });
    }

    public update(filter: any, update: any): Promise<UpdateWriteOpResult> {

        return this.collection.updateMany(filter, update);
    }

    public updateWithId(id: ObjectID, update: any): Promise<UpdateWriteOpResult> {

        return this.collection.updateOne({_id: id}, update);
    }

    public insert(insert: any): Promise<InsertOneWriteOpResult<any>> {

        return this.collection.insertOne(insert);
    }

    public inserts(inserts: any[]): Promise<InsertWriteOpResult<any>> {

        return this.collection.insertMany(inserts);
    }

    public delete(filter: any): Promise<DeleteWriteOpResultObject> {

        return this.collection.deleteMany(filter);
    }

    public deleteWithId(id: ObjectID): Promise<DeleteWriteOpResultObject> {

        return this.collection.deleteMany({_id: id});
    }
}
