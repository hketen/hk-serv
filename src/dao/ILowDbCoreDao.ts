/**
 * Created by hakan on 04/07/2017.
 */

export interface ILowDbCoreDao<T> {

    readonly collectionName: string;

    getCollection(): any;

    getItem(filter: any): T | null;

    getItemWithId(_id: string): T | null;

    getItems(filter: any): T[] | null;

    update(filter: any, update: T): void;

    updateWithId(_id: string, update: any): any;

    insert(insert: T): any;

    inserts(insert: T[]): any;

    delete(filter: any): any;

    deleteWithId(_id: string): any;
}
