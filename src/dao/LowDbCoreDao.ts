import {AdapterSync, LowdbSync} from 'lowdb';

import {ILowDbCoreDao} from './ILowDbCoreDao';

export class LowDbCoreDao<T> implements ILowDbCoreDao<T> {

    public readonly collectionName: string;
    protected db: LowdbSync<AdapterSync>;

    public constructor(db: LowdbSync<AdapterSync>, collectionName: string) {
        this.db = db;
        this.collectionName = collectionName;
    }

    getCollection(): any {
        return this
            .db
            .read()
            .get(this.collectionName);
    }

    delete(filter: any = {}): Promise<void> {
        return this.getCollection()
            .remove(filter)
            .write();
    }

    deleteWithId(id: string): Promise<void> {
        return this.delete({_id: id});
    }

    getItem(find: any = {}): T | null {
        return this.getCollection()
            .find(find)
            .cloneDeep()
            .value();
    }

    getItemWithId(_id: string): T | null {
        return this.getItem({_id});
    }

    getItems(filter: any = {}): T[] {
        return this.getCollection()
            .filter(filter)
            .cloneDeep()
            .value() || [];
    }

    insert(insert: any): any {
        return this.inserts([insert]);
    }

    inserts(insert: any[]): void {
        return this.getCollection()
            .push(...insert)
            .write();
    }

    update(find: any = {}, update: T): void {
        this.getCollection()
            .find({...find})
            .assign({...update})
            .write();
    }

    updateWithId(_id: string, update: T): void {
        this.update({_id}, update);
    }
}
