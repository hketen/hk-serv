/**
 * Created by hakan on 04/07/2017.
 */

import {DeleteWriteOpResultObject, InsertOneWriteOpResult, InsertWriteOpResult, ObjectID, UpdateWriteOpResult} from 'mongodb';

export interface IMongoCoreDao<T> {
    readonly collectionName: string;

    getItem(filter: any, opts?: object): Promise<T | null>;

    getItemWithId(id: ObjectID, opts?: object): Promise<T | null>;

    getItems(aggregateFilter: any, opts?: object): Promise<T[] | null>;

    update(filter: any, update: any): Promise<UpdateWriteOpResult>;

    updateWithId(id: ObjectID, update: any): Promise<UpdateWriteOpResult>;

    insert(insert: any): Promise<InsertOneWriteOpResult<any>>;

    inserts(insert: any[]): Promise<InsertWriteOpResult<any>>;

    delete(filter: any): Promise<DeleteWriteOpResultObject>;

    deleteWithId(id: ObjectID): Promise<DeleteWriteOpResultObject>;
}
