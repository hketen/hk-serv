
export interface IEmailConfig {
    name: string;
    host: string;
    port: number;
    authUser: string;
    authPass: string;
    secure: boolean;
}
