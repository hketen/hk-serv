/**
 * Created by hakan on 04/07/2017.
 */

export interface IHttpApplication {

    registerRoutes(): Promise<any[]>;

    injectRequest(): Promise<{ [key: string]: any }>;

}
