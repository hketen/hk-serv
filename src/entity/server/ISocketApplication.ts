/**
 * Created by hakan on 04/07/2017.
 */
import {SocketNamespace} from '..';

export interface ISocketApplication {

    registerSockets(): Promise<SocketNamespace[]>;

}
