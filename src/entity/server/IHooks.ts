/**
 * Created by hakan on 04/07/2017.
 */

export interface IHooks {

    beforeConfigHooks(): Promise<void>;

    afterConfigHooks(): Promise<void>;

    listenHooks(): Promise<void>;

}
