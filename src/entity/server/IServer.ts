/**
 * Created by hakan on 04/07/2017.
 */

export interface IServer {

    readonly publicFolder: string;
    readonly ipAddress: string;
    readonly port: number;
    readonly viewsFolder: string;

    parsePort(): number;

    parseIpAddress(): string;

}
