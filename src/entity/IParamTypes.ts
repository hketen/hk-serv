export interface IParamTypes {
    exp: string;
    type: string;
    paramName: string;
    key: string;
    defaultValue?: string;
}
