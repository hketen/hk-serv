/**
 * Created by hakan on 02/10/2019.
 */

export class SocketEventEntity {

    event: string;
    fnName: string;

    constructor(item: SocketEventEntity) {
        this.event = item.event;
        this.fnName = item.fnName;
    }

}
