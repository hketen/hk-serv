/**
 * Created by hakan on 04/07/2017.
 */
import {Socket} from 'socket.io';

import {SocketEntity} from './SocketEntity';

export class SocketNamespace {

    namespace: string;
    middleware?: Array<(socket: Socket, next: (err?: any) => void) => void>;
    controllers: SocketEntity[];

    constructor(item: SocketNamespace) {
        this.namespace = item.namespace;
        this.middleware = item.middleware || [];
        this.controllers = item.controllers;
    }

}
