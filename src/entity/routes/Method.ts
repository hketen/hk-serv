/**
 * Created by hakan on 02/10/2019.
 */

export enum Method {

    get = 'get',
    post = 'post',
    put = 'put',
    patch = 'patch',
    delete = 'delete'
}
