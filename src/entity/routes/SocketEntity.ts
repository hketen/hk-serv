/**
 * Created by hakan on 02/10/2019.
 */
import {Packet} from 'socket.io';

import {SocketEventEntity} from './SocketEventEntity';

export class SocketEntity {

    CurrentClass: any;
    middleware?: Array<(packet: Packet, next: (err?: any) => void) => void>;
    events: SocketEventEntity[];

    constructor(item: SocketEntity) {
        this.CurrentClass = item.CurrentClass;
        this.middleware = item.middleware || [];
        this.events = item.events;
    }

}
