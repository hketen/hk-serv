/**
 * Created by hakan on 02/10/2019.
 */

export class BlueprintMiddlewareEntity {

    all?: any[];
    item?: any[];
    items?: any[];
    create?: any[];
    destroy?: any[];
    update?: any[];

}
