/**
 * Created by hakan on 02/10/2019.
 */

export interface IBlueprintRoutes {

    item(): Promise<void>;

    items(): Promise<void>;

    create(): Promise<void>;

    update(): Promise<void>;

    partialUpdate(): Promise<void>;

    destroy(): Promise<void>;

}
