/**
 * Created by hakan on 02/10/2019.
 */

import {Method} from './Method';

export class RouteParamEntity {

    method: Method;
    routePath: string | RegExp;
    fnName: string;
    middleware?: any[];

    constructor(item: RouteParamEntity) {
        this.method = item.method;
        this.routePath = item.routePath;
        this.fnName = item.fnName;
        this.middleware = item.middleware;
    }

}
