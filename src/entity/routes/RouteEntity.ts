/**
 * Created by hakan on 02/10/2019.
 */
import {Method} from './Method';

export class RouteEntity {

    method: Method;
    routePath: string | RegExp;
    actions: any[];

    constructor(item: RouteEntity) {
        this.method = item.method;
        this.routePath = item.routePath;
        this.actions = item.actions;
    }
}
