export interface ITask {
    _id: string;
    module: string;
    date: Date;
    cron: string;
}
