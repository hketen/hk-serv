export interface IServerConfig {

    publicFolder: string;
    viewsFolder: string;
    ipAddress: string;
    port: number;

}
