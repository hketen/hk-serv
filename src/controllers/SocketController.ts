import {Server, Socket} from 'socket.io';

/**
 * Created by hakan on 04/07/2017.
 */

export abstract class SocketController {

    public io: Server;
    public socket: Socket;

    constructor(io: Server, socket: Socket) {
        this.io = io;
        this.socket = socket;
    }

    /**
     * ConnectionController
     *    - Controller
     */

}
