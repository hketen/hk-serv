/**
 * Created by hakan on 04/07/2017.
 */
import {ValuesController} from './http-controllers/ValuesController';

export abstract class HttpController extends ValuesController {

    /**
     * RequestController
     *  - ResponseController
     *    - NextController
     *       - UserRoleController
     *         - DbController
     *           - WSController
     *              - ParamController
     *                - UploaderController
     *                    - CryptoController
     *                        - ValuesController
     *                            - Controller
     */
}
