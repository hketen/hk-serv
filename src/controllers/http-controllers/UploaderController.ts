/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';
import {unlinkSync} from 'fs';
import _ from 'lodash';
import moment from 'moment';
import multer, {diskStorage, Field, StorageEngine} from 'multer';
import path from 'path';

import {IExt} from '../../entity';
import {ErrorModel} from '../../models';
import {Config, Converter, mkdir, pathResolve, rootPathResolve} from '../../utils';

import {ParamController} from './ParamController';

const supportedExt: IExt[] = Config.get('files.supportedExt') || [];
const uploadPath: string = rootPathResolve(Config.get('files.uploadPath') || '/uploads');

export abstract class UploaderController extends ParamController {

    protected uploadCtrl: multer.Instance;

    protected constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.uploadCtrl = this.getUpload();
    }

    protected getUpload(): multer.Instance {

        const storage: StorageEngine = diskStorage({
            destination: (req: any, file: any, callback: any): void => {
                callback(null, uploadPath);
            },
            filename: (req: any, file: any, callback: any): void => {
                const ext: string = path.extname(file.originalname);
                const fileName: string = path.basename(file.originalname, ext);
                callback(null, `${Date.now()}-${_.kebabCase(fileName)}${ext}`);
            },
        });

        const maxExt: IExt | undefined = _.maxBy(supportedExt, (item: IExt) => Converter.sizeToBytes(item.size));

        return multer({
            limits: {
                fileSize: Converter.sizeToBytes((maxExt && maxExt.size) || '35MB'),
            },
            storage,
        });
    }

    protected async uploadHandler(fields: string | Field[]): Promise<void> {

        await mkdir(uploadPath);

        return new Promise((resolve: () => void, reject: (error: any) => void): void => {

            if (_.isString(fields)) {

                this.uploadCtrl.single(fields)(this.request, this.response, (err: any) => err ? reject(err) : resolve());

            } else {

                this.uploadCtrl.fields(fields)(this.request, this.response, (err: any) => err ? reject(err) : resolve());

            }
        });
    }

    protected getFiles(fieldsName: string[]): { uploaded: any[], notUploaded: any[] } {

        const files: any[] = [];

        for (const name of fieldsName) {

            const fileNames: any[] = (this.request as any).files[name] || [];

            if (fileNames.length > 0) {
                files.push(...fileNames);
            }
        }

        const uploaded: any[] = [];
        const notUploaded: any[] = [];

        for (const file of files) {
            const isError: any = this.checkFile(file);

            if (isError) {
                const filePath: string = pathResolve(uploadPath, file.filename);

                unlinkSync(filePath);

                notUploaded.push({
                    originalname: file.originalname,
                    error: isError.error,
                    details: isError.details
                });
            } else {
                uploaded.push({
                    createdTime: moment().utc().format(),
                    originalname: file.originalname,
                    filename: file.filename,
                    mimetype: file.mimetype,
                    size: file.size
                });
            }
        }

        return {uploaded, notUploaded};
    }

    protected checkFile(file: { originalname: string, size: number }): undefined | ErrorModel {
        const fileExt: string = path.extname(file.originalname);

        const extItem: IExt | undefined = _.find(supportedExt, {ext: fileExt});

        if (!extItem) {
            return new ErrorModel('UNSUPPORTED_MEDIA_TYPE', 415);
        }

        const limit: number = Converter.sizeToBytes(extItem.size);
        if (file.size >= limit) {
            return new ErrorModel('ALLOWABLE_SIZE_LIMIT', 400, `LIMIT_${limit}`);
        }

        return;
    }

}
