/**
 * Created by hakan on 04/07/2017.
 */
import {IParamTypes} from '../../entity';

import {CryptoController} from './CryptoController';

export abstract class ValuesController extends CryptoController {

    private $values: any = {};

    protected setValues(values?: object): this {
        this.$values = values || {};
        return this;
    }

    protected addValue(name: string, value?: any): this {
        this.$values[name] = value;
        return this;
    }

    protected removeValue(name: string): this {
        delete this.$values[name];
        return this;
    }

    protected setValuesWithParams(names: string[], roles?: string | string[]): this {
        for (const name of names) {
            const opts: IParamTypes = this.paramTypes(name);
            if (!this.hasParam(opts.paramName)) {
                continue;
            }

            if (roles) {
                if (this.isRoles(roles)) {
                    this.$values[opts.key] = this.getParam(opts);
                }
                // izin verilmeyen parametreler bu noktada boşa düşer.
            } else {
                this.$values[opts.key] = this.getParam(opts);
            }
        }
        return this;
    }

    protected getValues(): any {
        return {...this.$values};
    }

}
