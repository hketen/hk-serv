/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';
import _ from 'lodash';
import {ObjectID} from 'mongodb';

import {IParamTypes} from '../../entity';
import {ErrorModel} from '../../models';
import {isUndefinedOrEmpty} from '../../utils';

import {WSController} from './WSController';

export abstract class ParamController extends WSController {

    protected parameters: any;

    protected constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);
        this.parameters = _.extend({}, req.body, req.query, req.params);
    }

    protected paramTypes(exp: string): IParamTypes {
        const splited: string[] = exp.split('|');
        const result: any = {};
        switch (splited.length) {
            case 3:
                // values içerisine hangi isimle kaydedileceğini belirler.
                result.key = splited[2];
                // requestten gelen parametre ismini belirler
                result.paramName = splited[1];
                // request ile gelen parametrenin tipini belirler.
                result.type = splited[0];
                break;
            case 2:
                result.key = splited[1];
                result.paramName = splited[1];
                result.type = splited[0];
                break;
            case 1:
                result.key = splited[0];
                result.paramName = splited[0];
                break;
        }
        return result;
    }

    protected getParam(opts: string | IParamTypes): any {
        if (_.isString(opts)) {
            opts = this.paramTypes(opts);
        }

        const val: any = this.hasParam(opts.paramName) ? this.parameters[opts.paramName] : opts.defaultValue;

        // boolean değerler string olarak gönderilmesine karşılık düzeltme işlemi
        if (val === 'true' || val === 'false') {
            return val === 'true';
        }

        try {
            switch (opts.type) {

                case 'id':
                    return val != null ? new ObjectID(val) : val;

                case '[?]':
                case '[id]':
                    return _
                        .chain(val)
                        .filter((v: string | undefined) => !_.isEmpty(v))
                        .map((v: string) => new ObjectID(v))
                        .value();

                case 'date':
                    return val != null ? new Date(val) : val;

                default:
                    return val;
            }
        } catch (e) {
            throw new ErrorModel('Desteklenmeyen Parametre (' + (opts.paramName) + ')', 400);
        }
    }

    protected getRoledParam(roles: string | string[], exp: string): any | undefined {

        if (this.isRoles(roles)) {
            return this.getParam(this.paramTypes(exp));
        }
    }

    protected hasParam(name: string): boolean {

        if (_.isString(this.parameters[name])) {
            this.parameters[name] = _.trim(this.parameters[name]);
            return this.parameters[name].length > 0;
        }

        return !_.isUndefined(this.parameters[name]);
    }

    protected requireParameters(paramNames: any[]): void {

        const notExists: string[] = [];

        for (const name of paramNames) {

            if (_.isArray(name)) {
                const orExists: string[] = [];

                for (const orName of name) {
                    if (isUndefinedOrEmpty(this.parameters[orName])) {
                        orExists.push(orName);
                    }
                }

                if (orExists.length === name.length) {
                    notExists.push(orExists.join('|'));
                }
            } else {
                if (isUndefinedOrEmpty(this.parameters[name])) {
                    notExists.push(name);
                }
            }
        }

        if (notExists.length === 1) {

            throw new ErrorModel('NOT_FOUND_PARAMETER_(' + notExists[0] + ')', 400);

        } else if (notExists.length > 1) {

            throw new ErrorModel('NOT_FOUND_PARAMETERS_(' + notExists.join(',') + ')', 400);

        }
    }

}
