/**
 * Created by hakan on 04/07/2017.
 */
import {Response} from 'express';

import {RequestController} from './RequestController';

export abstract class ResponseController extends RequestController {

    protected response: Response;

    protected constructor(req: any, res: Response) {
        super(req);
        this.response = res;
    }

    view(viewName: string, options: any = {}): void {
        this.response.render(viewName, options);
    }

    send(status: number = 200, data?: any): Response {

        return this.response.status(status).send(data);
    }

    ok(data: any): Response {
        return this.send(200, data);
    }

    authorizationError(data?: any): Response {
        return this.send(403, data);
    }

    authenticationError(data?: any): Response {

        return this.send(401, data);
    }

    paramError(data?: any): Response {

        return this.send(400, data);
    }

    queryError(data?: any): Response {

        return this.send(400, data);
    }

    uploadError(data?: any): Response {

        return this.send(400, data);
    }

    bodyError(data?: any): Response {

        return this.send(400, data);
    }

    processError(data?: any): Response {

        return this.send(400, data);
    }

    duplicateError(data?: any): Response {

        return this.send(409, data);
    }

    databaseError(data?: any): Response {

        return this.send(500, data);
    }

    runtimeError(data?: any): Response {

        return this.send(500, data);
    }

    notFoundError(data?: any): Response {

        return this.send(404, data);
    }
}
