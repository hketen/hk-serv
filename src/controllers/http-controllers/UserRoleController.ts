/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';

import {NextController} from './NextController';

export abstract class UserRoleController extends NextController {

    protected user: any;
    private readonly token: string;

    protected constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);
        this.user = req.user || null;
        this.token = this.getHeaderParams('token');
    }

    public static isRole(user: any, role: string): boolean {
        return ((user && user.roles) || []).indexOf(role) >= 0;
    }

    public static isRoles(user: any, roles: string | string[]): boolean {

        if (typeof roles === 'string') {

            return UserRoleController.isRole(user, roles);

        }

        for (const role of roles) {

            if (UserRoleController.isRole(user, role)) {
                return true;
            }
        }

        return false;
    }

    protected isRoles(roles: string | string[]): boolean {
        return UserRoleController.isRoles(this.user, roles);
    }

    protected getToken(): string {
        return this.token;
    }

    private getHeaderParams(name: string): any {
        return this.request.headers[name] || null;
    }
}
