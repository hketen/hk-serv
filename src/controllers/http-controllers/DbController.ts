/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';

import {UserRoleController} from './UserRoleController';

export abstract class DbController extends UserRoleController {

    protected db: any;

    protected constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);
        this.db = req.db;
    }
}
