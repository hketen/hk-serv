/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';

import {ResponseController} from './ResponseController';

export abstract class NextController extends ResponseController {

    protected next: NextFunction;

    protected constructor(req: any, res: Response, next: NextFunction) {
        super(req, res);
        this.next = next;
    }

}
