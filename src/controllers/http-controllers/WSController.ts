/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';
import {Server} from 'socket.io';

import {DbController} from './DbController';

export abstract class WSController extends DbController {

    protected io: Server;

    protected constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);
        this.io = req.io;
    }
}
