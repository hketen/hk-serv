/**
 * Created by hakan on 04/07/2017.
 */
import {Request} from 'express';

export abstract class RequestController {

    protected request: Request;

    protected constructor(req: any) {
        this.request = req;
    }

    protected setSession(key: string, value: any): void {
        if (this.request.session) {
            this.request.session[key] = value;
        }
    }

    protected getSession(key: string): any {
        if (this.request.session) {
            return this.request.session[key];
        }
    }
}
