/**
 * Created by hakan on 04/07/2017.
 */
import {ObjectID} from 'bson';
import CryptoJS from 'crypto-js';

import {UploaderController} from './UploaderController';

export abstract class CryptoController extends UploaderController {

    protected getCryptoPassword(userId: ObjectID, password: string): string {
        return this.getCryptoString(password + userId.toString());
    }

    protected getCryptoString(...strs: string[]): string {
        return CryptoJS.MD5(strs.join('')).toString();
    }

}
