/**
 * Created by hakan on 04/07/2017.
 */
/**
 * Created by hakan on 25/09/2018.
 */

import _ from 'lodash';
import moment from 'moment';
import schedule, {Job} from 'node-schedule';

import {ITask} from '../entity';
import {ErrorModel} from '../models';

export abstract class CoreTask {

    protected constructor() {
    }

    public static isExists(name: string): boolean {
        return !_.isUndefined(schedule.scheduledJobs[name]);
    }

    public static add(task: ITask, callback: () => {}): void {
        const name: string = task['_id'].toString();
        CoreTask.remove(name);

        if (task.date) {
            schedule.scheduleJob(name, task.date, callback);
        } else if (task.cron) {
            schedule.scheduleJob(name, task.cron, callback);
        } else {
            throw new ErrorModel('Date and Cron undefined.');
        }
    }

    public static remove(name: string): void {
        if (CoreTask.isExists(name)) {
            const job: Job = schedule.scheduledJobs[name];
            job.cancel();
        }
    }

    protected static getStopDate(stopDate: string): moment.Moment {
        if (stopDate === 'infinity') {
            return moment().utc().add(1, 'years');
        }

        return moment(stopDate).utc();
    }
}
