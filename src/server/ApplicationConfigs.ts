/**
 * Created by hakan on 04/07/2017.
 */

import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express, {json, NextFunction, Response, urlencoded} from 'express';
import session, {MemoryStore} from 'express-session';
import helmet from 'helmet';
import _ from 'lodash';
import uuid from 'uuid';

import {IHooks} from '../entity';
import {invalidParameters, loggers} from '../handlers';
import {toAsyncMiddleware, tokenPrepare, xPoweredBy} from '../middleware';

import SocketApplication from './SocketApplication';

export default abstract class ApplicationConfigs extends SocketApplication implements IHooks {

    public sessionStore: MemoryStore;
    public sessionSecretKey: string = 'NodeJS-Server';
    public sessionMaxAge: number = Date.now() + (7 * 86400 * 1000);

    public corsOptions: cors.CorsOptions | cors.CorsOptionsDelegate;

    protected constructor() {
        super();

        this.sessionStore = new MemoryStore();

        this.corsOptions = {
            credentials: true,
            origin: true
        };
    }

    protected async config(): Promise<void> {

        this.app.locals._ = _;
        this.app.locals.uuid = uuid;

        this.app
            .use(helmet({
                contentSecurityPolicy: false
            }))
            .all('*', xPoweredBy)

            // views config.
            .set('views', this.viewsFolder)
            .set('view engine', 'ejs')

            // compression (zlib)
            .use(compression())

            // sessions
            .use(session({
                store: this.sessionStore,
                genid: (): string => uuid.v1(),
                secret: this.sessionSecretKey || 'NodeJS',
                resave: true,
                saveUninitialized: true,
                cookie: {maxAge: this.sessionMaxAge, secure: false}
            }))

            // use logger middlware
            .use(loggers())
            // use json form parser middlware
            .use(json())
            // use query string parser middlware
            .use(urlencoded({extended: true}))

            .use(cookieParser())
            .use(invalidParameters())
            // add static paths
            .use(express.static(this.publicFolder))

            // static dosyalara token olmadan da erişebilirler.
            .all('*', tokenPrepare)

            // buraya kadar ulaşan bağlantı API bağlantısıdır. cors erişimi ve veritabanı bağlantısı gerektirir.
            .all('*', cors(this.corsOptions))
            .all('*', toAsyncMiddleware(async (req: any, res: Response, next: NextFunction) => {

                const injects: { [key: string]: any } = await this.injectRequest();

                _.extend(req, {
                    ...injects,
                    io: this.io
                });
                next();
            }))
        ;
    }

    async beforeConfigHooks(): Promise<void> {
    }

    async afterConfigHooks(): Promise<void> {
    }

    async listenHooks(): Promise<void> {
    }

}
