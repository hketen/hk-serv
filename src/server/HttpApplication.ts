/**
 * Created by hakan on 04/07/2017.
 */

import {Router} from 'express';
import http from 'http';
import {flattenDeep, forEach, isArray} from 'lodash';

import {IHttpApplication, RouteEntity} from '../entity';

import ExpressApplication from './ExpressApplication';

export default abstract class HttpApplication extends ExpressApplication implements IHttpApplication {

    protected httpServer: http.Server;

    protected constructor() {
        super();

        this.httpServer = http.createServer(this.app);
    }

    protected async useRoutes(): Promise<void> {

        const routers: any[] = await this.registerRoutes();

        const flatRouters: RouteEntity[] = flattenDeep(routers);

        if (isArray(flatRouters) && flatRouters.length > 0) {

            console.log('Binding Route Count:', flatRouters.length);

            const router: Router = Router();

            forEach(flatRouters, ({method, routePath, actions}: RouteEntity): void => {

                router[method](routePath, ...actions);
            });

            this.app.use(router);
        }

    }

    async registerRoutes(): Promise<any[]> {
        return [];
    }

    async injectRequest(): Promise<{ [key: string]: any }> {
        return {};
    }

}
