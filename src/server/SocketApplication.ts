/**
 * Created by hakan on 04/07/2017.
 */

import {flattenDeep, reduce} from 'lodash';
import socketIO, {Client, Namespace, Packet, Server, Socket} from 'socket.io';

import {ISocketApplication, SocketEntity, SocketEventEntity, SocketNamespace} from '../entity';

import HttpApplication from './HttpApplication';

export default abstract class SocketApplication extends HttpApplication implements ISocketApplication {

    protected io: Server;

    protected constructor() {
        super();

        this.io = socketIO(this.httpServer);
    }

    protected async useSockets(): Promise<void> {
        const namespaces: SocketNamespace[] = flattenDeep(await this.registerSockets());

        const eventCount: number = reduce(namespaces, (total: number, {controllers}: SocketNamespace) => {

            return reduce(
                flattenDeep(controllers),
                (t: number, {events}: SocketEntity) => events.length + t,
                total
            );

        }, 0);

        console.log('Binding Socket Event Count:', eventCount);

        for (const {namespace, middleware, controllers} of namespaces) {

            this.bindNamespace(
                new SocketNamespace({
                    namespace,
                    middleware,
                    controllers: flattenDeep(controllers)
                })
            );

        }
    }

    private bindNamespace({namespace, middleware, controllers}: SocketNamespace): void {

        if (controllers.length > 0) {

            const io: Namespace = reduce(middleware, (
                before: Namespace,
                item: (socket: Socket, next: (err?: any) => void) => void
            ) => before.use(item), this.io.of(namespace));

            io.on('connection', (socket: Socket) => {
                const flatControllers: SocketEntity[] = flattenDeep(controllers);

                this.bindControllers(socket, flatControllers);
            });
        }
    }

    private bindControllers(socket: Socket, controllers: SocketEntity[]): void {

        for (const {CurrentClass, middleware, events} of controllers) {

            const newClass: any = new CurrentClass(this.io, socket);
            this.onEvents(socket, newClass, middleware, events);
        }
    }

    private onEvents(socket: Socket, newClass: any, middleware: any, events: SocketEventEntity[]): void {

        const currentSocket: Socket = reduce(middleware, (
            before: Socket,
            item: (packet: Packet, next: (err?: any) => void) => void
        ) => before.use(item), socket);

        for (const {event, fnName} of events) {
            currentSocket.on(event, async (client: Client) => {
                await newClass[fnName](client);
            });
        }
    }

    async registerSockets(): Promise<SocketNamespace[]> {
        return [];
    }

}
