/**
 * Created by hakan on 04/07/2017.
 */

import ApplicationConfigs from './ApplicationConfigs';

export abstract class ApplicationServer extends ApplicationConfigs {

    /*
     * ExpressApplication implements IServer, IHooks
     *      HttpApplication implements IHttpApplication
     *          SocketApplication implements ISocketApplication
     *              ApplicationConfigs implements IHooks
     *                  ApplicationServer
     */

    public constructor() {
        super();
    }

    public async listen(): Promise<void> {

        await this.beforeConfigHooks();
        await this.config();
        await this.afterConfigHooks();

        await this.useRoutes();
        await this.useSockets();

        // error handler
        this.initErrorHandler();

        return new Promise((resolve: () => void, reject: (err: any) => void): void => {

            this.httpServer.on('error', (error: NodeJS.ErrnoException): void => {
                if (error.syscall !== 'listen') {
                    throw error;
                }
                let text: string = 'Unknown Error!';

                switch (error.code) {
                    case 'EACCES':
                        text = `${this.port} requires elevated privileges`;
                        break;
                    case 'EADDRINUSE':
                        text = `${this.port} is already in use`;
                        break;
                    default:
                        throw error;
                }

                reject(text);
                console.error(text);
                process.exit(1);
            });

            this.httpServer.listen(this.port, this.ipAddress, async (): Promise<void> => {
                console.log(`Listening on HTTP ${this.port}`);

                await this.listenHooks();

                resolve();
            });
        });

    }

}
