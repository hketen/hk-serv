/**
 * Created by hakan on 04/07/2017.
 */

import express from 'express';
import _ from 'lodash';

import {IServer, IServerConfig} from '../entity';
import {errorHandler, xhrErrorHandler} from '../handlers';
import {notFound} from '../middleware';
import {Config, rootPathResolve} from '../utils';

const defaultConfig: any = (): IServerConfig => {

    const server: any = Config.get('server');

    return {
        port: _.result(server, 'port', 8080),
        ipAddress: _.result(server, 'ipAddress', '0.0.0.0'),
        publicFolder: _.result(server, 'publicFolder', 'public'),
        viewsFolder: _.result(server, 'viewsFolder', 'views'),
    };
};

const {port, ipAddress, publicFolder, viewsFolder}: IServerConfig = defaultConfig();

export default abstract class Application implements IServer {

    readonly ipAddress: string;
    readonly port: number;
    readonly publicFolder: string;
    readonly viewsFolder: string;

    protected app: express.Application;

    protected constructor() {

        this.publicFolder = rootPathResolve(publicFolder);
        this.viewsFolder = rootPathResolve(viewsFolder);
        this.port = this.parsePort();
        this.ipAddress = this.parseIpAddress();

        this.app = express();
    }

    protected initErrorHandler(): void {
        this.app
            .use(xhrErrorHandler())
            .use(errorHandler())
            .get('*', notFound)
        ;
    }

    public parsePort(): number {
        const val: string | number = process.env.PORT || port || 8080;
        return (typeof val === 'string') ? parseInt(val, 10) : val;
    }

    public parseIpAddress(): string {
        return (process.env.IP || ipAddress || '0.0.0.0').toString();
    }

}
