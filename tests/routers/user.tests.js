const assert = require('chai').assert;
const request = require('request');
const config = require('../config.tests.json');

describe('API Service Method Calls "/"', function () {
    this.timeout(15 * 1000);

    it('GET("/") Call', function (done) {

        request({
            ...config.options,
            url: '/'
        }, function (error, response) {

            if (error) {
                done('error: ' + error.message);
                return;
            }


            if(!response){
                done("error response undefined.");
                return;
            }

            const {statusCode, statusMessage, body} = response;


            assert.equal(statusCode, 200, 'Status 200 değil');
            done();
        });
    });

});
