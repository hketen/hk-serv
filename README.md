

#NodeJS restful & socket server

The project was created with typescript.

##How To Use

```
import {ApplicationServer} from 'hk-serv';

class App extends ApplicationServer {

    async injectRequest(): Promise<{ [key: string]: any }> {
        const injects: { [key: string]: any } = {};

        ...

        return injects;
    }

    async registerRoutes(): Promise<any[]> {
        return []; // CoreRoutes.class
    }

    async registerSockets(): Promise<any[]> {
        return []];
    }
}

(async () => {

    const app = new App();

    await app.listen();

})();
```

## Webpack
```
...
externals: [
      nodeExternals({
        whitelist: ['hk-serv']
      }),
],
...
```

```
...
module: {
    rules: [
        ...
        {
            test: /.ts?$/,
            use: [
              {
                loader: 'ts-loader',
                options: {
                  allowTsInNodeModules: true // IMPORTANT
                }
              }
            ]
        },
        ...
    ]
},
...
```
